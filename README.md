# PIV Techniques for bubble tracking
  Set of techniques based on OpenCV are implemented in order to perform PIV using bubbles
 as tracers. This code is intended to overcome the performance limitations of more famous
 libraries (openPIV) implemented in Python, and to tap the full potential of computer
 vision libraries (such as openCV).

## How to run

   You should have your images in a separate folder, organized lexicographically. The location
   of the calibration board must be outside of said folder, and have the name 'BOARD.jpg'.

   Usage:

	$> PIV <directory> --xscale=<float> --yscale=<float> --freq=<float>

   The xscale,yscale variables represent the dimensions of the calibration board, and
   freq the frequency used to capture the images.

### Pre-requisites

   * OpenCV v.4.x.x.
   * CMake
   * C++17-conformant compiler

### Author

* **Santiago Lopez Castano** -- Researcher in FHR (Flanders Hydraulic Research)

### License

 * This project is Licensed under the MIT License (see LICENSE.md)  
