#include "utilities.h"
#include <opencv2/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>



using namespace cv;
using namespace std;

namespace utilities {
    Mat flowToHSV(Mat& flowInput)
    {
        CV_Assert(flowInput.depth() == CV_32F && flowInput.channels() == 2);
        Mat flow_parts[2];
        split(flowInput, flow_parts);
        Mat magnitude, angle, magn_norm;
        cartToPolar(flow_parts[0], flow_parts[1], magnitude, angle, true);
        normalize(magnitude, magn_norm, 0.0f, 1.0f, NORM_MINMAX);
        angle *= ((1.f / 360.f) * (180.f / 255.f));
        //build hsv image
        Mat _hsv[3], hsv, hsv8, bgr;
        _hsv[0] = angle;
        _hsv[1] = Mat::ones(angle.size(), CV_32F);
        _hsv[2] = magn_norm;
        merge(_hsv, 3, hsv);
        hsv.convertTo(hsv8, CV_8U, 255.0);
        cvtColor(hsv8, bgr, COLOR_HSV2BGR);
        return bgr;
    }
    Mat flowToVectorColormap(Mat& flowInput, int xstep, int ystep, float XSCALEVEL, float YSCALEVEL)
    {
        CV_Assert(flowInput.depth() == CV_32F && flowInput.channels() == 2);

        Mat flow_parts[2];
        split(flowInput, flow_parts);
        Mat magnitude(flowInput.size(), CV_32FC1);
        for (int i = 0; i < magnitude.rows; ++i)
            for (int j = 0; j < magnitude.cols; ++j)
                magnitude.at<float>(i, j) = sqrt(pow(XSCALEVEL * flow_parts[0].at<float>(i, j), 2.0) + pow(YSCALEVEL * flow_parts[1].at<float>(i, j), 2.0));

        double min, max;

        minMaxLoc(magnitude, &min, &max);

        Mat magn_norm, magn;
        normalize(magnitude, magn_norm, 0.0f, 255.0f, NORM_MINMAX);
        magn_norm.convertTo(magn, CV_8U);

        Mat falseColor;
        applyColorMap(magn, falseColor, cv::COLORMAP_JET);

        Mat colorBar(cv::Size(COLORBARL, falseColor.rows), CV_8UC1);
        for (int i = 0; i < colorBar.rows; ++i)
            for (int j = 0; j < colorBar.cols; ++j)
                colorBar.at<uchar>(i, j) = 255 - 255 * i / colorBar.rows;

        applyColorMap(colorBar, colorBar, cv::COLORMAP_JET);

        String toMax(to_string(max).substr(0, 5) + " m/s");
        String toMin(to_string(min).substr(0, 5) + " m/s");
        String toMean(to_string((max + min) * .5).substr(0, 5) + " m/s");

        putText(colorBar, toMax, Point(5, 10), FONT_HERSHEY_COMPLEX, .3, Scalar(0));
        putText(colorBar, toMin, Point(5, falseColor.rows - 5), FONT_HERSHEY_COMPLEX, .3, Scalar(0));
        putText(colorBar, toMean, Point(5, falseColor.rows / 2), FONT_HERSHEY_COMPLEX, .3, Scalar(0));


        int rowStep = flow_parts[0].rows/xstep;
        int colStep = flow_parts[0].cols/ystep;
        /*
        for (int i = rowStep / 2; i < flow_parts[0].rows; i += rowStep)
            for (int j = colStep / 2; j < flow_parts[0].cols; j += colStep)
            {
                Point pt1(i, j);
                Point pt2(i + flow_parts[0].at<float>(i, j), j + flow_parts[1].at<float>(i, j));
                arrowedLine(falseColor, pt1, pt2, Scalar(0), 1, 8, 0, .4);
            }
          */
        Mat tmp_color;
        hconcat(falseColor, colorBar, tmp_color);
        return tmp_color;
    }

}
/*
#define SZ_CHKBRD 20.0 
void CalculateRotationAndTranslation
(
    InputOutputArray Checkerboard,
    InputArray camMatr,
    InputArray distCoeffs,
    const vector<int> CHECKERBOARD,
    OutputArray rotationMatrix,
    OutputArray rotVec,
    OutputArray transVec
)
{
    CV_Assert(Checkerboard.depth() == CV_8U && Checkerboard.channels() == 1 && !Checkerboard.empty());

    // Defining the world coordinates for 3D points
    std::vector<cv::Point3f> objp;
    for (int i{ 0 }; i < CHECKERBOARD[1]; i++)
    {
        for (int j{ 0 }; j < CHECKERBOARD[0]; j++)
            objp.push_back(cv::Point3f(float(CHECKERBOARD[2] * j), float(CHECKERBOARD[2] * i), 0.0f));
    }

    // vector to store the pixel coordinates of detected checker board corners 
    std::vector<cv::Point2f> corner_pts;
    // find calibration points

    bool success = cv::findChessboardCorners
    (
        Checkerboard,
        cv::Size(CHECKERBOARD[0], CHECKERBOARD[1]),
        corner_pts,
        cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FAST_CHECK | cv::CALIB_CB_NORMALIZE_IMAGE
    );

    if (success)
    {
        cv::TermCriteria criteria(cv::TermCriteria::EPS | cv::TermCriteria::MAX_ITER, 15, 0.001);

        // refining pixel coordinates for given 2d points.
        cv::cornerSubPix(Checkerboard, corner_pts, cv::Size(11, 11), cv::Size(-1, -1), criteria);

        // Displaying the detected corner points on the checker board
        cv::drawChessboardCorners(Checkerboard, cv::Size(CHECKERBOARD[0], CHECKERBOARD[1]), corner_pts, success);

        cv::namedWindow("Input", cv::WINDOW_AUTOSIZE);
        cv::imshow("Input", Checkerboard);
        cv::waitKey(0);
    }
    else
        throw cv::Exception();

    cv::solvePnP(objp, corner_pts, camMatr, distCoeffs, rotVec, transVec);

    cv::Rodrigues(rotVec, rotationMatrix);
}
    Mat realFlowField(flowInput.size(), CV_32FC2);

    MatIterator_<Vec2f> it, end;
    for (it = flowInput.begin<Vec2f>(), end = flowInput.end<Vec2f>(); it != end; ++it)
    {
        Mat xyPoint = (Mat_<float>(3,1) <<  (*it)[0], (*it)[1], 1.0f);

        Mat convertedXYPoint = rotationMatrix.inv()*(camMatr.inv()*xyPoint - transVec)
    }
 */