#ifndef _HEADER_OPTICALFLOW
#define _HEADER_OPTICALFLOW

#include <opencv2/core.hpp>
#include <vector>
#include <string>
#define COLORBARL 64

namespace utilities {


    cv::Mat flowToHSV(cv::Mat& flowInput);
    cv::Mat flowToVectorColormap(cv::Mat& flowInput, int xstep, int ystep, float XSCALEVEL, float YSCALEVEL);
    std::vector<std::string> list_directory(const std::string& directory, const std::string& ext = ".bmp");


    /* this is kept here until fully implemented
    void CalculateRotationAndTranslation
    (
        cv::InputOutputArray Checkerboard,
        cv::InputArray camMatr,
        cv::InputArray distCoeffs,
        const std::vector<int> CHECKERBOARD,
        cv::OutputArray rotationMatrix,
        cv::OutputArray rotVec,
        cv::OutputArray transVec
    );
    */
}
#endif
