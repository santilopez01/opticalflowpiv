#include "utilities.h"

#ifdef _MSC_VER 
#	include "dirent.h"
#	include <Windows.h> // This thing breaks OpenCV in ways unimaginable, Dont put near any openCV's #include
#else
#	include <regex>
#	ifndef __has_include
		static_assert(false, "__has_include not supported");
#	else
#		if __cplusplus >= 201703L && __has_include(<filesystem>)
#			include <filesystem>
			namespace fs = std::filesystem;
#		elif __has_include(<experimental/filesystem>)
#			include <experimental/filesystem>
			namespace fs = std::experimental::filesystem;
#		elif __has_include(<boost/filesystem.hpp>)
#			include <boost/filesystem.hpp>
			namespace fs = boost::filesystem;
#		endif
#	endif
#endif


#include <stdexcept>

namespace utilities {

/*
** Word of Caution:
    each of these routines return different things, EVEN IN DIFFERENT WINDOWS/POSIX/LINUX VERSIONS
        in POSIX the first two 'files' are '.' and '..'
    The Windows version is "cleaner": you can select the extension of the files you want to import
        e.g: list_directory_win(dir,".bmp");
    Why didn't I used <filesystem>? Well, because Windows, since in VSC doesn't fucking work (2019).
        If you are lucky enough to have ISO-C++17, then use <filesystem> and disregard this stupid hack.
** Santiago
*/
#ifdef _MSC_VER 
    std::vector<std::string> list_directory(const std::string& directory, const std::string& ext)
    {
        WIN32_FIND_DATAA findData;
        HANDLE hFind = INVALID_HANDLE_VALUE;
        std::string full_path = directory + "\\*" + ext;
        std::vector<std::string> dir_list;

        hFind = FindFirstFileA(full_path.c_str(), &findData);

        if (hFind == INVALID_HANDLE_VALUE)
            throw std::runtime_error("Invalid path: " + directory);

        while (FindNextFileA(hFind, &findData) != 0)
        {
            dir_list.push_back(std::string(findData.cFileName));
        }

        FindClose(hFind);

        return dir_list;
    }
/*
    // in the future, add 'ext' 
    std::vector<std::string> list_directory_posix(const std::string& directory)
    {
        DIR* dir;
        struct dirent* ent;
        std::vector<std::string> ret;

        if ((dir = opendir(directory.c_str())) != nullptr) {
            /* print all the files and directories within directory */
            while ((ent = readdir(dir)) != nullptr) {
                ret.emplace_back(ent->d_name);
            }
            closedir(dir);
        }
        else
            throw std::runtime_error("Invalid path: " + directory);
        return ret;
    }
*/
#endif

#ifdef __GNUG__
	bool regex_predicate(const std::string& a, const std::string& b, const std::string& ext)
	{
		std::regex rx(std::string("_ms[0-9]+\\")+ext);
		std::cmatch m;

		std::regex_search(a.c_str(),m,rx);
		std::string astrip(m[0]);

		std::regex_search(b.c_str(),m,rx);
		std::string bstrip(m[0]);
		
		return astrip < bstrip;
	} 

	std::vector<std::string> list_directory ( const std::string& directory, const std::string& ext)
	{
		std::vector<std::string> ret;

		for(const auto& file : fs::directory_iterator(directory) )
		{
			if(!fs::is_regular_file(file))
				continue;
			if(file.path().extension().string() == ext)
				ret.push_back(file.path().string());
			/*
			std::string name(file.path().filename().string());
			if(name.find(ext) != std::string::npos)
				ret.push_back(name);
			*/
		}
		sort(ret.begin(),ret.end(), 
			[&ext](const std::string& a, const std::string& b){
				return regex_predicate(a,b,ext);
			});
		return ret;
	}
#endif
}
