#ifndef PIVIMAGE_H
#define PIVIMAGE_H

#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <cassert>
#include <vector>

//- Class that condenses all transforms done to a single class
//- Used to "simplify" the conversion towards video (NOT ELEGANT) 
namespace PIV {

class PIVImage {
public:
	//- Constructors

	PIVImage()
		: original_(), warped_() {}

	explicit PIVImage(cv::Mat& in)
		: original_(in), warped_() {}

	explicit PIVImage(std::string& file, cv::ImreadModes imType = cv::IMREAD_GRAYSCALE)
		: original_(imread(file, imType)), warped_()
	{
		assert(!original_.empty());
	}

	//- destructors

	~PIVImage() = default;

	//- Setters and Getters

	cv::Mat& Image()
	{
		assert(!empty());
		if (!isWarped())
			return original_;
		return warped_;
	}

	bool empty()
	{
		return original_.empty();
	}

	bool isWarped()
	{
		return !warped_.empty();
	}
	//- Methods that work on warped_

	void planeCropAndTransform(std::vector<cv::Point2f>& ROI_);

	//void orthogonalize(...) --> Implement

	void Sharpen();

	void Crop(cv::Rect& x);

	//- Methods that modify original_

	void overwrite(std::string& file, cv::ImreadModes imType = cv::IMREAD_GRAYSCALE)
	{
		original_ = imread(file, imType);
		assert(!empty());
		if (!warped_.empty()) {
			warped_.release();
		}
	}
	void overwrite(cv::Mat& M)
	{
		original_ = M;
		assert(!empty());
		if (!warped_.empty()) {
			warped_.release();
		}
	}
private:

	//- Data
	cv::Mat original_; // raw image --> defines state of class (always exists!)
	cv::Mat warped_;   // result of transformations/filtering --> if undefined, return original_

};
}
#endif
