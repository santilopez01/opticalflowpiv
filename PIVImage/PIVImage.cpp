#include "PIVImage.h"

using namespace cv;
using namespace std;
using namespace PIV;

void PIVImage::planeCropAndTransform(vector<Point2f>& ROI_)
{
    CV_Assert(ROI_.size() == 4 || ROI_.empty());
    if (!ROI_.empty()) {
        vector< Point2f> midpoints(4);
        vector< Point2f> dst_corners(4);

        midpoints[0] = (ROI_[0] + ROI_[1]) / 2;
        midpoints[1] = (ROI_[1] + ROI_[2]) / 2;
        midpoints[2] = (ROI_[2] + ROI_[3]) / 2;
        midpoints[3] = (ROI_[3] + ROI_[0]) / 2;
        dst_corners[0].x = 0;
        dst_corners[0].y = 0;
        dst_corners[1].x = (float)norm(midpoints[1] - midpoints[3]);
        dst_corners[1].y = 0;
        dst_corners[2].x = dst_corners[1].x;
        dst_corners[2].y = (float)norm(midpoints[0] - midpoints[2]);
        dst_corners[3].x = 0;
        dst_corners[3].y = dst_corners[2].y;

        Size warped_image_size = Size(cvRound(dst_corners[2].x), cvRound(dst_corners[2].y));
        Mat M = getPerspectiveTransform(ROI_, dst_corners);
        warpPerspective(original_, warped_, M, warped_image_size); // do perspective transformation
    }
}

void PIVImage::Sharpen()
{
    Mat ORG;
    Mat out;
    if (isWarped())
        ORG = warped_;
    else
        ORG = original_;
    bilateralFilter(ORG, out, -1, 2, .25);
    warped_ = out;
}

void PIVImage::Crop(Rect& r)
{
    warped_ = Mat(original_, r);
}
